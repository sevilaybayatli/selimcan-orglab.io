if(typeof Math.imul == "undefined" || (Math.imul(0xffffffff,5) == 0)) {
    Math.imul = function (a, b) {
        var ah  = (a >>> 16) & 0xffff;
        var al = a & 0xffff;
        var bh  = (b >>> 16) & 0xffff;
        var bl = b & 0xffff;
        // the shift by 0 fixes the sign on the high part
        // the final |0 converts the unsigned value into a signed value
        return ((al * bl) + (((ah * bl + al * bh) << 16) >>> 0)|0);
    }
}


            ;var b=this||self;function e(a){var c=a.length;if(0<c){for(var h=Array(c),d=0;d<c;d++)h[d]=a[d];return h}return[]};function f(){this.c=""}f.prototype.toString=function(){return"SafeScript{"+this.c+"}"};f.prototype.a=function(a){this.c=a};(new f).a("");function g(){this.g=""}g.prototype.toString=function(){return"SafeStyle{"+this.g+"}"};g.prototype.a=function(a){this.g=a};(new g).a("");function k(){this.f=""}k.prototype.toString=function(){return"SafeStyleSheet{"+this.f+"}"};k.prototype.a=function(a){this.f=a};(new k).a("");function l(){this.b=""}l.prototype.toString=function(){return"SafeHtml{"+this.b+"}"};l.prototype.a=function(a){this.b=a};(new l).a("\x3c!DOCTYPE html\x3e");(new l).a("");(new l).a("\x3cbr\x3e");var m={},n={};if("undefined"===typeof m||"undefined"===typeof n||"undefined"===typeof p)var p={};if("undefined"===typeof m||"undefined"===typeof n||"undefined"===typeof q)var q=null;if("undefined"===typeof m||"undefined"===typeof n||"undefined"===typeof r)var r=null;if("undefined"===typeof m||"undefined"===typeof n||"undefined"===typeof t)var t=null;if("undefined"!==typeof Symbol){var u=Symbol;"object"!=typeof u||!u||u instanceof Array||u instanceof Object||Object.prototype.toString.call(u)}
var v="undefined"!==typeof Math&&"undefined"!==typeof Math.imul&&0!==Math.imul(4294967295,5)?function(a,c){return Math.imul(a,c)}:function(a,c){var h=a&65535,d=c&65535;return h*d+((a>>>16&65535)*d+h*(c>>>16&65535)<<16>>>0)|0};function w(a){a=v(a|0,-862048943);a=0^(v(a<<15|a>>>-15,461845907)|0);a=(v(a<<13|a>>>-13,5)+-430675100|0)^0;a=v(a^a>>>16,-2048144789);v(a^a>>>13,-1028477387)}w(1);w(0);if("undefined"===typeof m||"undefined"===typeof n||"undefined"===typeof x)var x=null;
"undefined"!==typeof console&&(q=function(){return console.log.apply(console,e(arguments))},r=function(){return console.error.apply(console,e(arguments))});if("undefined"===typeof m||"undefined"===typeof n||"undefined"===typeof y)var y=function(){throw Error("cljs.core/*eval* not bound");};function z(a){return document.getElementById(a).play()}var A=["hello_world","core","playSound"],B=b;A[0]in B||"undefined"==typeof B.execScript||B.execScript("var "+A[0]);for(var C;A.length&&(C=A.shift());)A.length||void 0===z?B=B[C]&&B[C]!==Object.prototype[C]?B[C]:B[C]={}:B[C]=z;