(ns hello-world.core)

(defn ^:export playSound [sound]
  (.play (.getElementById js/document sound)))
