#lang pollen

@h3{Aç kurt [1]}

@p{Bir varmış, bir yokmuş. Allah’ın kulu çokmuş. Çok söylemesi günahmış; hikâye
söylemesi sevapmış. Evvel zaman içinde, kalbur saman içinde bir kurt
yaşarmış. Köyün kıyısında kışları açlıktan kıvranıyormuş. Yine böyle bir gün:}

@p{- Köye gideyim de oradaki inekten, koyundan yiyeyim, demiş.}

@p{Köye gitmiş, bir ineğe rast gelmiş:}

@p{- İnek, ben öyle açıktım ki seni yiyeceğim, demiş.}

@p{- Dur, beni şimdi yeme. Şuraya kadar sırtıma bin, in; birbirimizi gezdirelim
de beni öyle ye, demiş.}

@p{Kurt kabul etmiş ve o sırada da inek kaçmış. Kurt bir ahırın önüne gitmiş.
Ahırdan bir katır çıkmış. Katıra:}

@p{- Açlıktan ölüyorum, katır seni yiyeceğim, demiş.}

@p{- Benim etim sert, sen beni yiyemezsin. Gideyim, baltayla satırı getireyim
de beni öyle ye, demiş.}

@p{Baltayla satıra gidiyorum diye katır da kaçmış. Kurt, av aramaya devam
etmiş. Bir koyuna rastlamış:}

@p{- Koyun, açlıktan ölüyorum, seni yiyeceğim, demiş.}

@p{- Yok, beni şimdi yeme. Gel, seninle şu tarafa doğru gidelim de orada bir
oynayalım, demiş.}

@p{O da kurdu kandırıp kaçmış. Sonra kurt, keçiyle karşılaşmış. Keçiye:}

@p{- Seni yiyeceğim keçi, çok açım, demiş. Keçi:}

@p{- Benim karnımda iki tane yavrum var. Bizi üç olunca ye, demiş.}

@p{Sonra o da kaçmış. Kurt harmanlığa doğru yoluna devam etmiş. Bir ata rast
gelmiş. Ata:}

@p{- At, açlıktan ölüyorum. İmkânı yok, kaçırmam seni; seni yiyeceğim, demiş.
At:}

@p{- Yok, beni şimdi yeme. Gel, sırtıma bin de bir cirit oynayalım. Beni ondan
sonra ye, demiş.}

@p{Böylece at da kaçmış. Kurt bütün avlarını kaçırmış. Bu sefer düşünmeye ve
kendi kendine söylenmeye başlamış:}

@p{- Be hey kurt! Eline geçti bir inek, ye de boynuzlarını dinelt. Sen ne yapa-
    caksın inmeyi, binmeyi? Kâtip mi olduydun, demiş. Katırı düşünmüş:}

@p{- Eline geçti bir katır, yesene hatır hatır. Sen ne yapacaksın baltayı,
satırı?  Kasap mı olacaktın, demiş. Sonra koyunu düşünmüş:}

@p{- Hey kafasız! Sen ne edeceksin oyunu moyunu, yesene koyunu. Oynayıp da
köçek başı mı olacaktın, demiş. Oradan keçi gelmiş aklına:}

@p{- Eline geçti bir keçi, ne yapacaksın ikiyi, üçü; kessene keçiyi. Sürü başı
mı olacaktın yoksa, demiş. Sonra atı düşünmüş:}

@p{- Eline geçti bir at, ye de yanında yat. Sen ne yapacaksın cirit oynamayı?
Cirit başı mı olacaktın, demiş.}

@p{Bütün avlarını kaçıran kurt, açlıktan ölmüş.}

@p{Hatun KARAMUKLU}
