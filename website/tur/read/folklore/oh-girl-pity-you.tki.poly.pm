#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@(require (rename-in (only-in "texts/oh-girl-pity-you.tur.pm" doc)
           (doc TUR)))
@(require (rename-in (only-in "texts/oh-girl-pity-you.tki.pm" doc)
           (doc TKI)))

@(for/splice ([tur (cdr TUR)]
              [tki (cdr TKI)])
 @div[#:class "page"]{
  @div[#:class "lefthalf"]{@tur}
  @div[#:class "righthalf" #:dir "rtl" #:style "margin-right: 100px"]{@tki}})

@h3{Kaynaklar / _}

@ol{

@li{Necati Demir (2017) Anadolu masallarından derlemeler}

}
