#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "KhanAcademy . org / math Tatarça with a-la Frank subtitles (unofficial)")
@(define-meta author           "")

@(define LANG "tt")
@(define HEIGHT "480")

@p{Был биттә математикаға өйрәтеүсе, @a[#:href "https://www.khanacademy.org" #:target "_blank"]{Хан Академияһы} әҙерләгән видеолар һәм күнегеү@numbered-note{Күнегеү һылтамалар ғына ҡуйясаҡбыҙ.} тупланған.}

@margin-note{Беренсе һәм өсөнсө төр субтитрларны беҙ (йәғни selimcan.org) әҙерләнек.}

@p{Һәр видео өс төрлө форматта биренәсәк: 1) @a[#:href "http://www.franklang.ru/index.php/metod-chteniya-ili-franka" #:target "_blank"]{Илья  Франк ысулы} буйынса адатацияләнгән тексттарға оҡшаған субтитрлар менән, 2) инглизсә субтитрлар менән, һәм 3) татарса субтитлар менән. Мәҫәлән, бына был: @kavideo[#:frank "https://www.youtube-nocookie.com/embed/6SF09CROAVI"
         #:english "https://www.youtube-nocookie.com/embed/y2-uaPiyoxc"
         #:tatar "https://www.youtube-nocookie.com/embed/y2-uaPiyoxc"]{@video-title{@u{@eng{Counting with small numbers} @tat{Кескенә һандар менән һанау}}}}}

@p{Өс төрлө cубтитрларның булыуы Һеҙгә (балағыҙға) бер уҡ ваҡытта инглизчәне да, математиканы да өйрәнергә ярҙам итер тигән өмөттә ҡалабыҙ.}

@p{Инглизчәне өйрәнә ғына башлаған булһағыз, беҙ Һеҙгә ул видеоларҙы килтеренгән тәртиптә ҡарарга кәңәш итәбеҙ (йәғни башта "Франкса" икетелле субтитрлар менән,@numbered-note{Был төр видеоларҙы ҡарағанат видеоны еш-еш туҡтатырга тура киләсәк лыр, сөнки субтитрлар бер-береһен бик тиҙ алыштыра. Бының өсөн видеоның теләһә ҡайһы еренә баҫыу да етә, нәҡ «пауза» төймәһенә ғына баҫыу мәжбүри түгел.} унан инглизсә субтитрлар менән, һәм, теләһәгез, татарса субтитрлар менән). Беренсе ҡат ҡарағанат субтитрларны сүндереп ҡарау да файҙалы --- инглиз һөйләмен аңларга тиҙерәк өйрәнәсәкһегеҙ.}

@p{Видеоны тулы экранда ҡарау өсөн, видеоның аҫҡы уң босмағындағы шаҡмаҡҡа баҫығыҙ йәки видеоға ике тапҡыр «сиртегеҙ».}

@section{Күнегеү}

@p{Видеоларҙан тыш, selimcan.org'да Хан Академияһы күнегеү һылтамалар күрерһегеҙ. Мәҫәлән:}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-counting/e/counting-out-1-20-objects"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Count with small numbers} @tat{Күнегеү: кескенә һандар менән һана}}}}}

@margin-note{@strong{Оҙон һүҙҙең ҡыҫҡаһы: әҙер дәрестәр исемлеген был биттең иң аҫҡы өлешендә күреп була @hyperlink["#lessons"]{⇣}}}

@p{Бер темаға кагылышлы видео һәм күнегеү йыйылмаһын беҙ "дәрес" тип атайбыҙ.}

@p{Күнегеү әлегә береһе да татар телендә юҡ, әммә тора-бара уларҙы да тәржемә итербеҙ тип торабыҙ, сөнки ул күнегеү --- Хан Академияһының иң көслө як береһе. @margin-note{Мәңге бушлай репетитор} Улар артында кызыклы алгоритмдәр тора, ул алгоритмдәр ярҙамында күнегеү һәр уҡыусыға яраклаша һәм ул уҡыусы өсөн виртуль бер репетитор роль уйнай, йәғни мәҫәлән.}

@section{Хан Академияһы материалдарын татарчага тәржемә итеү турында}

@p{Хан Академияһы сайты татарса да эшләй башлаһын өсөн (һәм күнегеү тәржемә итергә мөмкинлек пәйда булһын өсөн), иң тәүҙә @a[#:href "https://www.khanacademy.org/math" #:target "_blank"]{khanacademy.org/math}’ның кәмендә бер кисәгендәге (Early Math, Arithmetic, Albegra 1, Algebra 2 һ.б.) барлыҡ видеолар да тәржемә ителгән булырга тейеш@numbered-note{күберәк мәғлүмәтте
  @a[#:href "https://khanacademy.zendesk.com/hc/en-us/articles/226412428-What-are-our-goals-and-milestones-" #:target "_blank"]{ошонда} табып була.}.}

@p{Беҙ эште «Early Math» өлешендәге видеоларҙы тәржемә итеүнән башланыҡ, сөнки был иң кескенә нисекһәк.}

@p{Субтитрларны тәржемә итеү Хан Академияһының @a[#:href "https://www.youtube.com/channel/UCs8a-PNM8EHKKU28XQLetLw" #:target "_blank"]{рәсми Youtube каналында} башҡарына. Инглизсә да, татарса да яҡшы беләһегеҙ һәм субтитрларны тәржемә итергә ярҙам итәһегеҙ килә икән, күңелегеҙгә йә килгән һәм татарса субтитрлары булмаған видеоның аҫҡы уң сатындағы @img[#:src "icons/gear.svg" #:alt "сигезпочмак" #:height "15"] «Settings» билгеһенә, һәм унан һуң асылған менюла «Subtitles → Add subtitles» төймәләренә баҫығыҙ.}

@p{Тотошлай (йәғни тауышы да, видеолағы яҙыулар да) татар теленә тәржемә ителгән видеоларҙы @a[#:href "https://www.youtube.com/channel/UCVFdUw6YYN2cD5HHwt_JJSA" #:target "_blank"]{KhanAcademyTatarchaUnofficial} YouTube каналында ҡарап була.}

@p{@a[#:href "https://www.youtube.com/channel/UC606RHDIRGR_QLGvPYgO1Xw" #:target "_blank"]{Selimcan.org/Extra} YouTube каналында иһә өҫтәге беренсе төр видео шикелле, икетелле субтитрлы видеолар тупланған.}

@h2[#:id "lessons"]{Дәрестәр}

@ol{
  @li{@smallcaps{@hyperlink["counting.html"]{Һанау}}}
  @li{@smallcaps{@hyperlink["numbers-0-to-120.html"]{0-дән алып 120-гә кадәрге һандар}}}
  @li{@smallcaps{@hyperlink["counting-objects.html"]{Кешеләрҙе һанау}}}
}

@p{Бер дәрестән икенсеһенә күсеү өсөн һәр биттең өҫкө һул сатындағы "Next →"
төймәһенә баҫығыҙ.}
