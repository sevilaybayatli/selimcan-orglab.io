#lang pollen

@(define-meta chapter-num      "1")
@(define-meta title            "The Old Sea Dog at the Admiral Benbow")

@u{@eng{Squire Trelawney, Dr Livesey} @tat{сквайр@numbered-note{squire — алпавыт} Трелони, доктор Ливси},} @u{@eng{and the rest of these gentlemen} @tat{һәм бу әфәнделәрнең калганнары}} @u{@eng{having asked me to write down} @tat{миннән кәгазьгә төшереүмне сорагач}} @u{@eng{the whole particulars about Treasure island} @tat{Хәзинә Утравы турындагы барлык нечкәлекләрне},} @u{@eng{from the beginning to the end} @tat{башыннан алып ахырына кадәр},} @u{@eng{keeping nothing back but the bearings of the island} @tat{утрауның урнашуыннан кала һичнәрсәне дә яшермичә},} @u{@eng{and that only because there is still treasure not yet lifted} @tat{һәм бусы да анда әле казып алынмаган хәзинә булганга гына; @de["to lift"]{күтәрү, казып алу}}} @u{@eng{I take up my pen} @tat{мин @ell{кулыма} каләмемне алам}} @u{@eng{in the year of grace 17—} @tat{Гайсә пәйгамбәр тууыннан 17.. нче елда},} @u{@eng{and go back to the time} @tat{һәм шул вакытка әйләнеп кайтам}} @u{@eng{when my father kept the @np{Admiral Benbow} inn} @tat{кайчан минем әтием @np{Адмирал Бенбоу}@numbered-note{адмирал Бенбоу (1653–1702) — инглизләрнең мәшһүр диңгез каһарманнарының берсе} трактирын тота иде},} @u{@eng{and the brown old seaman} @tat{һәм @eq["көрән"]{кояшта каралган} карт диңгезче},} @u{@eng{with the sabre cut} @tat{кылычтан калган @eq["киселүле"]{яралы}},} @u{@eng{first took up his lodging under our roof} @tat{беренче тапкыр безнең түбә астында @eqr["үз торагын алды"]{урнашты}}.}

Squire Trelawney, Dr Livesey, and the rest of these gentlemen having asked me to write down the whole particulars about Treasure island, from the beginning to the end, keeping nothing back but the bearings of the island, and that only because there is still treasure not yet lifted, I take up my pen in the year of grace 17—, and go back to the time when my father kept the @np{Admiral Benbow} inn, and the brown old seaman, with the sabre cut, first took up his lodging under our roof.

@par{@u{@eng{I remember him as if it were yesterday}
  @tat{мин аны бу кичә булган кебек хәтерлим},}
 @u{@eng{as he came plodding to the inn door}
  @tat{ничек ул авыр атлап трактир ишегенә килде;
   @de["to plod"]{авыр атлау, аякларны өстерәү}},}
 @u{@eng{his sea-chest following behind him in a handbarrow}
  @tat{@eq["аның диңгез сандыгы аның артыннан барган килеш бер куларбасында"]{
    куларбасына куелган диңгез сандыгын өстерәп;}
   @de["hand"]{кул, кул чугы}; @de["barrow"]{арба}};}
 @u{@eng{a tall, strong, heavy, nut-brown man}
  @tat{биек буйлы, көчле, авыр, кояшта каралган кеше;
   @de["nut-brown"]{каштан төсе}; @de["nut"]{чикләвек}};}
 @u{@eng{his tarry pigtail falling over the shoulders}
  @tat{аның майланган @eqr["дуңгыз койрыгы"]{толымы} җилкәсенә төшеп тора;
   @de["tarry"]{дегетле}; @de["tar"]{дегет}}}
 @u{@eng{of his soiled blue coat}
  @tat{керләнгән, зәңгәр төстәге кафтанынының};}
 @u{@eng{his hands ragged and scarred}
  @tat{куллары кытыршы һәм яра эзләре белән тулы},}
 @u{@eng{with black, broken nails}
  @tat{кара, сынык тырнаклы};}
 @u{@eng{and the sabre cut across one cheek}
  @tat{һәм бер яңагы аша кылыч эләгүдән калган яра эзе;
   @de["across"]{аркылы, аша}},}
 @u{@eng{a dirty, livid white}
  @tat{пычрак, көлсу-ак @ell{төстәге}}.}
 @u{@eng{I remember him looking round the cove}
  @tat{диңгез култыгын карап чыкканын хәтерлим}}
 @u{@eng{and whistling to himself as he did so}
  @tat{һәм ул моны эшләгәндә үзалдына сызгыргалаганын},}
 @u{@eng{and then breaking out in that old sea-song}
  @tat{һәм аннан соң кычкырып шул борынгы матрос җырын җырлап җибәргәнен;
   @de["to break out in"]{көтмәгәндә нидер эшли башлау (җырлый, көлә, елый һ.б.ш.)}}}
 @u{@eng{that he sang so often afterwards}
  @tat{кайсын ул аннан соң да шундый еш җырлады}:}
}

@verse{
 @par{@u{@eng{“Fifteen men on the dead man's chest}
   @tat{мәет сандыгына/сандыгында 15 адәм} —} @br{}
  @u{@eng{Yo-ho-ho, and a bottle of rum}
   @tat{йо-хо-хо, һәм бер шешә ром}!”}}}

@par{I remember him as if it were yesterday, as he came plodding to the inn door, his
 sea-chest following behind him in a handbarrow; a tall, strong, heavy, nut-brown
 man; his tarry pigtail falling over the shoulders of his soiled blue coat; his hands
 ragged and scarred, with black, broken nails; and the sabre cut across one cheek, a
 dirty, livid white. I remember him looking round the cove and whistling to himself
 as he did so, and then breaking out in that old sea-song that he sang so often
 afterwards:}

@verse{“Fifteen men on the dead man's chest — @br{}
 Yo-ho-ho, and a bottle of rum!”}

@par{@u{@eng{in the high, old tottering voice}
  @tat{югары, карт калтыравыклы тавыш белән}}
     @u{@eng{that seemed to have been tuned and broken at the capstan@numbered-note{capstan --- “кабестан” яки “шпиль” — кораб палубасына вертикаль рәвештә урнаштырылучы, тимердән я агачтан эшләнгән авыр барабан. Әлеге барабан борыслар-рычаглар ярдәмендә әйләндерелә, һәм ул барабанга чорналучы чылбырлар я арканнар авыр якорьларны күтәрә я төшерә.}@margin-figure["img/capstan-nautical.jpg"]{Кабестан рәсеме (Википедиядән)} bars}
  @tat{@eqr["кайсысы кабестанның борысларыннан чыга һәм ватыла кебек иде"]{кайсысы кабестанның борыслары шикелле шыгырдап чыга иде};
       @de["to break"]{сыну, туктау}}.}
     @u{@eng{Then he rapped on the door}
  @tat{аннан соң ул ишекне шакыды}}
 @u{@eng{with a bit of stick like a handspike}
  @tat{кабестанны әйләндәрә торган рычагка охшаган таяк белән;
   @de["handspike"]{кабестанны әйләнерү өчен кулланылучы, агачтан эшләнгән авыр борыс яки рычаг}}}
 @u{@eng{that he carried}
  @tat{кайсысын ул йөртә иде = кайсысы аның кулында иде},}
 @u{@eng{and when my father appeared}
  @tat{һәм минем әти @eq["күренгәч"]{килгәч}}},
 @u{@eng{called roughly for a glass of rum}
  @tat{илтифатсыз рәвештә бер стакан ром сорады}.}
 @u{@eng{This, when it was brought to him}
  @tat{моны (ромны), ул аңа китерелгәч; @de["to bring"]{алып килү, китерү}}}
 @u{@eng{he drank slowly}
  @tat{ул әкрен генә эчте; @de["to drink"]{эчү}},}
 @u{@eng{like a connoisseur}
  @tat{белгән кеше кебек},}
 @u{@eng{lingering on the taste}
  @tat{@eq["тәмендә озак калып"]{тәмен сузып}},}
 @u{@eng{and still looking about him}
  @tat{һаман да тирә-ягына каранып}}
 @u{@eng{at the cliffs and up at our signboard}
  @tat{кыяларга һәм өскә безнең трактир билгесенә}.}
}

@par{
 @u{@eng{“This is a handy cove}
  @rus{это удобная бухта}
  @tat{бу уңайлы бухта},”}
 @u{@eng{says he, at length}
  @rus{сказал он, наконец}
  @tat{@eq["ди"]{диде} ул, ниһаять};}
 @u{@eng{“and a pleasant situated grog-shop}
  @rus{и славно расположенная винная лавка;
   @de["grog"]{грог /горячий алкогольный напиток, разновидность пунша, приготовляемый на основе рома, коньяка или водки и горячей воды с сахаром и пряностями и употребляемый только в холодное время года/}}
  @tat{һәм шәп урнашкан шәрабханә;
   @de["grog"]{грог /салкын вакытта эчелә торган кайнар исерткеч эчемлек; ром я коньякка су, шикәр һәм төрле тәмләткечләр кушып ясала./}}.}
 @u{@eng{Much company, mate}
  @rus{много гостей = посетителей, приятель}
  @tat{кунаклар күпме, иптәш}?”}}

@par{
 @u{@eng{My father told him no}
  @rus{отец сказал ему, /что/ нет}
  @tat{әти аңа “юк” дип җавап бирде},}
 @u{@eng{very little company}
  @rus{очень мало народу}
  @tat{кунаклар бик аз},}
 @u{@eng{the more was the pity}
  @rus{тем хуже = к сожалению: «тем болшье была жалость»}
  @tat{кызганычка каршы;
   @de["more's the pity"]{(гыйбарә-идиома) сүзгә-сүз тәрҗемәсе: «аянычлыгы тагын да күбрәк», мәгънәсе: кызганычка каршы}.}}}

@par{in the high, old tottering voice that seemed to have been tuned and broken at the
 capstan bars. Then he rapped on the door with a bit of stick like a handspike that he
 carried, and when my father appeared, called roughly for a glass of rum.
 This, when it was brought to him, he drank slowly, like a connoisseur, lingering
 on the taste, and still looking about him at the cliffs and up at our signboard.}

@par{“This is a handy cove,” says he, at length; “and a pleasant situated grog-shop.
 Much company, mate?”}

@par{My father told him no, very little company, the more was the pity.}

@par{
 @u{@eng{“Well, then}
  @rus{итак}
  @tat{алайса},”}
 @u{@eng{said he}
  @rus{сказал он}
  @tat{диде ул},}
 @u{@eng{“this is the berth for me}
  @rus{эта якорная стоянка для меня}
  @tat{кунар урыным шушы булыр}.}
 @u{@eng{Here you matey}
  @rus{сюда, браток}
  @tat{бирегә, туган},”}
 @u{@eng{he cried to the man who trundled the barrow}
  @rus{крикнул он человеку, который катил тачку}
  @tat{куларбаны тәгәрәтүче кешегә кычкырды ул};}
 @u{@eng{“bring up alongside and help up my chest}
  @rus{греби сюда и помоги поднять = втащить мой сундук; to bring up — поставить или стать на якорь; alongside — по борту, рядом}
  @tat{монда таба иш һәм сандыгымны @eq["күтәрергә"]{кертергә} ярдәм ит;
   @de["to bring up alongside"]{(хәрбиләр телендә) бер корабны я.б.ш. тигез итеп икенчесе янәшәсенә кую}; @; https://forum.wordreference.com/threads/to-bring-it-up-alongside.1075421/
   @de["alongside"]{буйлап, янәшәсендә}}.}
 @u{@eng{I'll stay here a bit}
  @rus{я останусь здесь немного = ненадолго}
  @tat{мин бераз монда торачакмын},”}
 @u{@eng{he continued}
  @rus{продолжил он}
  @tat{@ell{дип} дәвам итте ул}.}
 @u{@eng{“I'm a plain man}
  @rus{я простой человек}
  @tat{мин гади кеше};}
 @u{@eng{rum and bacon and eggs is what I want}
  @rus{ром, бекон и яйца = яичница — /вот то/, что я хочу = все, что мне нужно}
  @tat{ром, бекон һәм йомырка — @eq["мин теләгән"]{миңа кирәкле} нәрсә шул},}
 @u{@eng{and that head up there for to watch ships off}
  @rus{тот мыс вон там, /хорошо подходит/, чтобы смотреть на корабли вдали /проходящие в море/}
  @tat{һәм әнә теге кыя тегендә, еракта @ell{диңгездән үтүче} корабларны күзәтер өчен}.}
 @u{@eng{What you mought call me}
  @rus{как вы можете называть меня; mought = might}
  @tat{миңа кем дип дәшә аласызмы; @eq["mought"]{might (диалекталь яисә архаик вариант)}}?}
 @u{@eng{You mought call me captain}
  @rus{можете называть меня капитаном}
  @tat{миңа капитан дип дәшә аласыз}.}
 @u{@eng{Oh, I see what you're at — there}
  @rus{понимаю, чего вы ждете — вот; to be at — намереваться}
  @tat{о, аңлыйм, ни көткәнегезне — әнә; @de["at"]{-да/-дә, өстендә}};”}
 @u{@eng{and he threw down three or four gold pieces on the threshold}
  @rus{и он бросил три или четыре золотые монеты на порог; piece — кусок; монета}
  @tat{һәм ул бусагага өч-дүрт алтын тәңкә ташлады;
   @de["to throw"]{ташлау};
   @de["piece"]{кисәк, тәңкә}}.}
 @u{@eng{“You can tell me when I've worked through that}
  @rus{можете сказать мне = обратиться, когда кончится это; to work — работать, использовать; through — через,полностью}
  @tat{бу беткәч, миңа әйтә аласыз;
   @de["to work"]{эшләү, бу очракта: сарыф итү};
   @de["through"]{аркылы}},”}
 @u{@eng{says he, looking as fierce as a commander}
  @rus{глядя так же свирепо, как командир = с видом командира}
  @tat{ди ул, командирларча усал карап}.}
}

@par{“Well, then,” said he, “this is the berth for me. Here you matey,” he cried to the
 man who trundled the barrow; “bring up alongside and help up my chest. I'll stay
 here a bit,” he continued. “I'm a plain man; rum and bacon and eggs is what I want,
 and that head up there for to watch ships off. What you mought call me? You
 mought call me captain. Oh, I see what you're at — there;” and he threw down three
 or four gold pieces on the threshold. “You can tell me when I've worked through
 that,” says he, looking as fierce as a commander.}

@par{@u{@eng{And, indeed}
  @tat{һәм, чыннан да},}
 @u{@eng{bad as his clothes were}
  @tat{киемнәре начар булса да}}
 @u{@eng{and coarsely as he spoke}
  @tat{һәм тупас сөйләшсә дә},}
 @u{@eng{he had none of the appearance of a man who sailed before the mast}
  @tat{@eq["аңарда мачта алдында йөзүче кеше кыяфәтеннән һичнәрсә дә юк иде"]{ул мачта алдында йөзүче кешегә охшамаган иде}; @expl{корабның мачта (корабның җилкән беркетелә торган “баганасы”) алдындагы өлешендә гади матрослар яшәсә, командирлары мачта артындагы өлешендә торганнар}};}
 @u{@eng{but seemed like a mate or skipper}
  @tat{ә капитан ярдәмчесе я шкиперга охшаган иде; @de["to seem"]{охшау}}}
 @u{@eng{accustomed to be obeyed or to strike}
  @tat{@eq["тыңлануга"]{аны тыңлауларына} я кыйнарга күнеккән}.}
 @u{@eng{The man who came with the barrow}
  @tat{куларба белән килгән кеше}}
 @u{@eng{told us the mail had set him down this morning before at the @np{Royal George}
   @tat{сөйләде безгә, почта @ell{атлары} аны бүген иртән @np{Royal George} @ell{кунакханәсенә} китерде @ell{дип}}}}
 @u{@eng{that he had inquired}
  @tat{ул сорашты дип}}
 @u{@eng{what inns there were along the coast}
  @tat{яр буенда нинди трактирлар булганын}}
 @u{@eng{and hearing ours well spoken of}
  @tat{һәм безнекенең яхшы сүз белән телгә алынганын ишеткәч},}
 @u{@eng{I suppose}
  @tat{дип уйлыйм},}
 @u{@eng{and described as lonely}
  @tat{һәм ялгыз @ell{урнашкан} дип тасвирланганын},}
 @u{@eng{had chosen it from the others for his place of residence}
  @tat{башкалар арасыннан аны сайлап алды үзенең яшәү урыны буларак дип}.}
 @u{@eng{And that was all we could learn of our guest}
  @tat{Һәм кунагыбыз хакында белә алганыбызның һәммәсе дә шушы иде}.}}

@par{And, indeed, bad as his clothes were, and coarsely as he spoke, he had none
 of the appearance of a man who sailed before the mast; but seemed like a mate or
 skipper accustomed to be obeyed or to strike. The man who came with the barrow
 told us the mail had set him down this morning before at the @np{Royal George};
 that he had inquired what inns there were along the coast, and hearing ours well
 spoken of, I suppose, and described as lonely, had chosen it from the others for
 his place of residence. And that was all we could learn of our guest.}

@par{@u{@eng{He was a very silent man by custom}
  @tat{ул @eq["гадәт буенча"]{гадәттә} бик сүзсез кеше иде}.}
 @u{@eng{All day he hung round the cove}
  @tat{көне буе бухта @ell{яры} буйлап йөри;
   @de["to hang a(round)"]{китми тору, вакыт үткәрү, тырай тибеп йөрү}},}
 @u{@eng{or upon the cliffs}
  @tat{я кыяларга @ell{менә} иде},}
 @u{@eng{with a brass telescope}
  @tat{җиз @s{телескобы} күзәтү көпшәсе белән};}
 @u{@eng{all evening he sat in a corner of the parlour next the fire}
  @tat{кич буе зал бүлмәсендәге @eq["ут"]{камин} янында утыра иде},}
 @u{@eng{and drank rum and water very strong}
  @tat{һәм ром белән су эчә иде @eq["бик көчле итеп"]{аз су кушып}}.}
 @u{@eng{Mostly he would not speak when spoken to}
  @tat{гадәттә аның белән сөйләшсәләр ул дәшми иде;
   @de["to speak to"]{сөйләшү}};}
 @u{@eng{only look up sudden and fierce}
  @tat{фәкать кинәт һәм усал итеп карый иде дә}}
 @u{@eng{and blow through his nose like a fog-horn}
  @tat{@eqr["томан сиренасы [өргән] кебек борыны аша өрә иде"]{томанда кораб быргысын сызгырткандай, борынын сызгыртып куя иде};
   @de["fog"]{томан}; @de["horn"]{мөгез, быргы}; @de["to blow"]{өрү}};}
 @u{@eng{and we and the people who came about our house}
  @tat{һәм без, һәм @eq["безнең йортка эләккән кешеләр"]{безнең йорттагы кунаклар}}}
 @u{@eng{soon learned to let him be}
  @tat{тиздән аны тынычлыкта калдырырга өйрәндек}.}
 @u{@eng{Every day}
  @tat{һәр көн},}
 @u{@eng{when he came back from his stroll}
  @tat{@eqr["ул үзенең җәяү йөрешеннән әйләнеп кайткач"]{ул җәяү йөреп кайткач}; @de["stroll"]{җәяү йөрү}},}
 @u{@eng{he would ask}
  @tat{ул сорый иде}}
 @u{@eng{if any seafaring men had gone by along the road}
  @tat{юл буйлап диңгезчеләр үтмәдеме дип}.}
 @u{@eng{At first we thought}
  @tat{башта без уйлый идек}}
 @u{@eng{it was the want of company of his own kind}
  @tat{бу аның үзе кебекләр арасында булу теләге иде дип}}
 @u{@eng{that made him ask this question}
  @tat{нәрсә аны мәҗбүр итә бу сорауны бирергә};}
 @u{@eng{but at last we began to see}
  @tat{әмма ниһаять без @eq["күрә"]{аңлый} башладык}}
 @u{@eng{he was desirous to avoid them}
  @tat{ул алар белән очрашмаска тырыша иде; @de["desirous"]{теләүче, омтылучы}}.}
 @u{@eng{When a seaman put up at the @np{Admiral Benbow}}
  @tat{берәр диңгезче @np{Адмирал Бенбоу}-да тукталса}}
 @u{@eng{(as now and then some did}
  @tat{@eq["ничек кайсылары ара-тирә эшли иде"]{һәм бәгъзеләре ара-тирә, чыннан да, туктала иде}},}
 @u{@eng{making by the coast road for Bristol}
  @tat{яр буендагы юл буйлап Бристольга барганда})}
 @u{@eng{he would look in at him through the curtained door}
  @tat{ул аңа башта ишек чаршавы аша @ell{гына} карый иде}}
 @u{@eng{before he entered the parlour}
  @tat{зал бүлмәсенә керер алдыннан};}
 @u{@eng{and he was always sure to be as silent as a mouse}
  @tat{һәм ул һәрвакыт тычкан кебек тып-тын була иде}}
 @u{@eng{when any such was present}
  @tat{андый @ell{кеше бүлмәдә} булганда}.}
}

@par{He was a very silent man by custom. All day he hung round the cove, or upon
 the cliffs, with a brass telescope; all evening he sat in a corner of the parlour next
 the fire, and drank rum and water very strong. Mostly he would not speak when
 spoken to; only look up sudden and fierce, and blow through his nose like a fog-
 horn; and we and the people who came about our house soon learned to let him be.
 Every day, when he came back from his stroll, he would ask if any seafaring men
 had gone by along the road. At first we thought it was the want of company of his
 own kind that made him ask this question; but at last we began to see he was
 desirous to avoid them. When a seaman put up at the @np{Admiral Benbow} (as now
 and then some did, making by the coast road for Bristol) he would look in at him
 through the curtained door before he entered the parlour; and he was always sure to
 be as silent as a mouse when any such was present.}

@par{
 @u{@eng{For me, at least}
  @tat{минем өчен, кимендә}}
 @u{@eng{there was no secret about the matter}
  @tat{бу мәсьәлә турында сер юк иде};}
 @u{@eng{for I was, in a way, a sharer in his alarms}
  @tat{@eq["чөнки мин, беркадәр, аның борчылуларында катнашучы идем"]{ул үзенең борчылуларын минем белән уртаклашты};
   @de["to share"]{уртаклашу, бүлешү}}.}
 @u{@eng{He had taken me aside one day}
  @tat{ул мине читкә чакырып алды бервакыт},}
 @u{@eng{and promised me a silver fourpenny}
  @tat{һәм миңа көмеш дүрт пеннилек @ell{тәңкә түләргә} вәгъдә итте;
   @expl{пенни я пенс — инглиз акчасы, фунт стерлингның ике йөз кырыктан бер өлеше}}}
 @u{@eng{on the first of every month}
  @tat{һәр айның бересендә}}
 @u{@eng{if I would only keep my @qt{weather-eye open for a seafaring man with one leg,}}
  @tat{@qt{әгәр дә мин бер аяклы диңгезче пәйда булмадымы икәнен игътибар белән күзәтеп йөрсәм,};
   @de["to keep eyes open; to keep weather eye open"]{сүзгә сүз: күзләрне ачык тоту; мәгънәсе: абай булу, ачык авыз булмау};
   @de["weather-eye"]{абайлылык, игътибарлылык, диккать}}}
 @u{@eng{and let him know the moment he appeared}
  @tat{һәм аңа хәбәр итсәм ул күренүгә үк}.}
 @u{@eng{Often enough}
  @tat{еш кына},}
 @u{@eng{when the first of the month came round}
  @tat{айның бересе җиткәч},}
 @u{@eng{and I applied to him for my wage}
  @tat{һәм мин аңа үземнең хезмәт хакым@ell{ны алыр} өчен мөрәҗәгать иткәч},}
 @u{@eng{he would only blow through his nose at me}
  @tat{ул миңа борынын гына сызгырта иде}}
 @u{@eng{and stare me down}
  @tat{һәм усал итеп карап куя иде;
   @de["to stare down"]{икенче кеше карашын читкә я аска алырлык итеп усал карау}};}
 @u{@eng{but before the week was out}
  @tat{әмма атна узганчы}}
 @u{@eng{he was sure to think better of it}
  @tat{яхшырак уйлап фикерен үзгәртмичә},}
 @u{@eng{bring me my fourpenny piece}
  @tat{миңа дүрт пенслы тәңкәне китермичә}}
 @u{@eng{and repeat his orders to look out for @qt{the seafaring man with one leg}}
  @tat{һәм @qt{бераяклы диңгезчене} күзәтергә кушуын кабатламыйча калмый иде}.}}

@par{For me, at least, there was no secret about the matter; for I was, in a way, a
 sharer in his alarms. He had taken me aside one day, and promised me a silver
 fourpenny on the first of every month if I would only keep my
 @qt{weather-eye open for a seafaring man with one leg,} and let him know the moment he appeared.
 Often enough, when the first of the month came round, and I applied to him for my
 wage, he would only blow through his nose at me, and stare me down; but before
 the week was out he was sure to think better of it, bring me my fourpenny piece,
 and repeat his orders to look out for @qt{the seafaring man with one leg.}}

@par{
 @u{@eng{How that personage haunted my dreams}
  @tat{ул адәмнең минем төшләремне ничек эзәрлекләгәнен},}
 @u{@eng{I need scarcely tell you}
  @tat{Сезгә әйтеп торуым кирәкмидер}.}
 @u{@eng{On stormy nights}
  @tat{давыллы төннәрдә},}
 @u{@eng{when the wind shook the four corners of the house}
  @tat{җил йортның @eq["дүрт чатын да"]{бар йортны да} дер селкеткәндә; @de["to shake"]{селкетү}},}
 @u{@eng{and the surf roared along the cove and up the cliffs}
  @tat{һәм бухта буенда һәм кыялар буйлап өскә таба дулкыннар шаулаганда},}
 @u{@eng{I would see him in a thousand forms}
  @tat{мин аны меңләгән шәкелдә күрә идем},}
 @u{@eng{and with a thousand diabolical expressions}
  @tat{һәм меңнәрчә җен-убыр кыяфәтендә}.}
 @u{@eng{Now the leg would be cut off at the knee}
  @tat{аның аягы бер вакыт тездән киселгән була},}
 @u{@eng{now at the hip}
  @tat{икенче вакыт боттан};}
 @u{@eng{now he was a monstrous kind of a creature}
  @tat{я гыйфрит төсле бер җан иясе була иде}}
 @u{@eng{who had never had but the one leg}
  @tat{кайсының бер аягыннан башка һичнәрсәсе булмый иде},}
 @u{@eng{and that in the middle of his body}
  @tat{һәм анысы да гәүдәсенең уртасында}.}
 @u{@eng{To see him leap and run and pursue me}
  @tat{аның сикерүен һәм йөгерүен һәм мине куалавын күрү}}
 @u{@eng{over hedge and ditch}
  @tat{койма һәм чокыр өстеннән}}
 @u{@eng{was the worst of nightmares}
  @tat{төшләрнең иң начары иде}.}
 @u{@eng{And altogether I paid pretty dear for my monthly fourpenny piece}
  @tat{һәм, гомумән, айлык дүрт пеннилык тәңкәм өчен мин шактый кыйммәт түләдем},}
 @u{@eng{in the shape of these abominable fancies}
  @tat{шушы коточкыч төшләр белән}.}}

@par{How that personage haunted my dreams, I need scarcely tell you. On stormy
 nights, when the wind shook the four corners of the house, and the surf roared
 along the cove and up the cliffs, I would see him in a thousand forms, and with a
 thousand diabolical expressions. Now the leg would be cut off at the knee, now at
 the hip; now he was a monstrous kind of a creature who had never had but the one
 leg, and that in the middle of his body. To see him leap and run and pursue me
 over hedge and ditch was the worst of nightmares. And altogether I paid pretty
 dear for my monthly fourpenny piece, in the shape of these abominable fancies.}

@par{
 @u{@eng{But though I was so terrified by the idea of the seafaring man with one leg}
  @tat{бер аяклы диңгезчене уйлаудан бик курыксам да}}
 @u{@eng{I was far less afraid of the captain himself}
  @tat{мин капитанның үзеннән азрак курка идем},}
 @u{@eng{than anybody else who knew him}
  @tat{аны белүче башка берәр кешегә караганда}.}
 @u{@eng{There were nights when he took a deal more rum and water}
  @tat{ул артыграк ром һәм су эчеп җибәргән төннәр була иде}}
 @u{@eng{than his head would carry}
  @tat{аның башы йөртә алганнан}}
 @u{@eng{and then he would sometimes sit and sing}
  @tat{һәм андый чакта ул кайвакыт утыра һәм җырлый иде};}
 @u{@eng{his wicked, old, wild sea-songs, minding nobody}
  @tat{үзенең усал, борынгы, кыргый диңгезче җырларын, беркемгә дә игътибар итмичә};}
 @u{@eng{but sometimes he would call for glasses round}
  @tat{әмма кайвакыт бездән һәркем өчен дә берәр стакан @ell{ром} китертә иде},}
 @u{@eng{and force all the trembling company to listen to his stories}
  @tat{һәм котлары алынып утырган мәҗлесне үзенең хикәяләрен тыңларга мәҗбүр итә иде}}
 @u{@eng{or bear a chorus to his singing}
  @tat{@eqr["я үзенең җырларына хор йөртергә"]{я үзенең җырларына кушылып җырларга}}.}
 @u{@eng{Often I have heard the house shaking with @qt{Yo-ho-ho, and a bottle of rum}}
    @tat{йортның @qt{Йо-һо-һо, һәм бер шешә ром} @ell{җырыннан} дер селкенүен еш ишетә идем};}
 @u{@eng{all the neighbours joining in for dear life}
    @tat{бөтен күршеләр дә @eqr["кадерле тормышлары өчен"]{бөтен көченә} кушылып @ell{җырлый-җырлый};
     @de["to join in"]{кушылу, берәр эшкә катышу}},}
 @u{@eng{with the fear of death upon them}
    @tat{өсләренә үлем килү куркусы белән},}
 @u{@eng{and each singing louder than the other, to avoid remark}
    @tat{һәм һәркайсы икенчесеннән кычкырыбрак җырлап, шелтәгә эләкмәс өчен}.}
 @u{@eng{For in these fits}
    @tat{чөнки ул @eq["өянәкләрдә"]{ярсуларда}}}
 @u{@eng{he was the most overriding companion ever known}
    @tat{ул @eq["кайчан да булса билгеле"]{тарихта булган} @eq["иң мөһим"]{иң дәһшәтле} әңгәмәдәш була иде;
     @de["overriding"]{1. иң мөһим 2. доминант}};}
 @u{@eng{he would slap his hand on the table for silence all round}
    @tat{тирә-якты тынычлык @ell{урнашсын} өчен кулы белән өстәлгә суга иде};}
 @u{@eng{he would fly up in a passion of anger at a question}
    @tat{@eq["сорауда"]{сорау бирелгәч} @eqr["ачу дәртендә очып китә иде"]{ярсып китә иде};
      @de["to fly up"]{очып китү, талпыну}}}
 @u{@eng{or sometimes because none was put}
    @tat{яисә кайвакыт бернинди дә @ell{сорау} бирелмәгәч},}
 @u{@eng{and so he judged the company was not following his story}
    @tat{һәм ул мәҗлес аның хикәясе @eq["артыннан бармый"]{игътибар белән тыңламый} дип уйлый иде}.}
 @u{@eng{Nor would he allow anyone to leave the inn}
    @tat{трактирдан китәргә дә рөхсәт итми иде; @de["to leave"]{китү}}}
 @u{@eng{till he had drunk himself sleepy and reeled off to bed}
    @tat{ул йокымсырый башлаганчы эчкәнче һәм ава-түнә ятагына киткәнче;
     @de["sleepy"]{йокылы, йокымсыраучы}}.}}

@par{But though I was so terrified by the idea of the seafaring man with one leg, I was
 far less afraid of the captain himself than anybody else who knew him. There were
 nights when he took a deal more rum and water than his head would carry; and
 then he would sometimes sit and sing his wicked, old, wild sea-songs, minding
 nobody; but sometimes he would call for glasses round, and force all the trembling
 company to listen to his stories or bear a chorus to his singing. Often I have heard
the house shaking with 'Yo-ho-ho, and a bottle of rum;' all the neighbours joining
in for dear life, with the fear of death upon them, and each singing louder than the
other, to avoid remark. For in these fits he was the most overriding companion ever
known; he would slap his hand on the table for silence all round; he would fly up
in a passion of anger at a question, or sometimes because none was put, and so he
judged the company was not following his story. Nor would he allow anyone to
leave the inn till he had drunk himself sleepy and reeled off to bed.}

@par{
 @u{@eng{His stories were what frightened people worst of all}
    @tat{кешеләрне иң нык куркытканы аның хикәяләре иде}.}
 @u{@eng{Dreadful stories they were}
    @tat{куркыныч хикәяләр иде алар};}
 @u{@eng{about hanging}
    @tat{@eq["асу"]{дар агачлары} турында}}
 @u{@eng{and walking the plank}
    @tat{һәм такта буйлап йөрү @ell{турында}; @expl{"такта буйлап йөрү" — пиратларның вәхши җәза төрләренең берсе: яулап алынган кораб диңгезчеләрен я башка, ярамаган эш кылган кешеләрне, кулларын һәм күзләрен бәйләп, бер очы корабтан диңгез өстенә чыгарып куелган такта буйлап йөрергә мәҗбүр иткәннәр}},}
 @u{@eng{and storms at sea}
    @tat{һәм диңгездәге штормнар @ell{турында}}}
 @u{@eng{and the Dry Tortugas}
    @tat{һәм @np{Драй Тортугас} @ell{утравы турында}; @expl{Драй Тортугас — Флорида штатының көньяк-көнбатышында, Мексикан култыгында урнашкан корал утраулары төркеме}}}
 @u{@eng{and wild deeds and places on the Spanish Main}
    @tat{һәм Испан Кыйтгасындагы @expl{Кариб диңгезе тирәләре шулай атала} кыргый эшләр һәм урыннар @ell{турында}}.}}

@par{
 @u{@eng{By his own account}
    @tat{үзенең хикәясенә күрә}}
 @u{@eng{he must have lived his life among some of the wickedest men}
    @tat{ул тормышын бәгъзе иң явыз кешеләр арасында үткәргән булырга @eq["тиеш"]{охшый}}}
 @u{@eng{that God ever allowed upon the sea}
    @tat{кайсыларына Аллаһы Тәгалә кайчан да булса диңгезгә @ell{чыгарга} ирек биргән};}
 @u{@eng{and the language in which he told these stories}
    @tat{һәм ул хикәяләрен сөйләгән тел дә}}
 @u{@eng{shocked our plain country people}
    @tat{безнең гади авыл кешеләрен шаккаттыра иде}}
 @u{@eng{almost as much as the crimes that he described}
    @tat{ул тасвирлаган җинаятьләр @ell{шаккатырган} кадәр диярлек}.}}

@par{His stories were what frightened people worst of all. Dreadful stories they were;
about hanging, and walking the plank, and storms at sea, and the Dry Tortugas, and
wild deeds and places on the Spanish Main.}

@par{By his own account he must have lived his life among some of the wickedest
men that God ever allowed upon the sea; and the language in which he told these
stories shocked our plain country people almost as much as the crimes that he
described.}
