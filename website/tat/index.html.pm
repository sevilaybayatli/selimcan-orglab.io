#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "Selimcan.org — бушлай онлайн дәреслекләр")
@(define-meta author           "Илнар Сәлимҗанов")

@h2{Эчтәлек}

@ol{

@li{@hyperlink["../tat/about.html"]{Selimcan.org турында}}
@li{@hyperlink["../tat/manual.html"]{Selimcan.org уку-укыту программасы турында бирелә торган кайбер сораулар һәм программаны куллану тәртибе}}
@li{Дәреслекләр}

@ol{

@li{@hyperlink["../tat/math/index.html"]{Математика}}
@li{@hyperlink["../tat/read/index.html"]{Уку}}
@li{Язу}}}
