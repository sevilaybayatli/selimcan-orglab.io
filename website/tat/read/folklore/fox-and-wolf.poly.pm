#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@(require (rename-in (only-in "texts/fox-and-wolf.tat.pm" doc)
           (doc FOX-AND-WOLF-TAT)))
@(require (rename-in (only-in "texts/fox-and-wolf.tur.pm" doc)
           (doc FOX-AND-WOLF-TUR)))

@(for/splice ([tat (cdr FOX-AND-WOLF-TAT)]
              [tur (cdr FOX-AND-WOLF-TUR)])
 @div[#:class "page"]{
  @div[#:class "lefthalf"]{@tat}
  @div[#:class "righthalf"]{@tur}})

@h3{Чыганаклар / Kaynaklar}

@ol{

@li{Гатина, Ярми (1977) Халык авыз иҗаты. Беренче том. Әкиятләр}

@li{Mustafa Gültekin (2010) Tataristan masallari üzerinde bir araştırma. T.C.
Ege üniversitesi sosyal bilimler enstitüsü Türk Halk Bilimi Anabilim dalı
(doktora tezi)}

}
