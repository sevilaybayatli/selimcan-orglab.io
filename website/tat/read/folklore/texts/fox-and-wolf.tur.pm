#lang pollen

@h3{Tilki ile kurt [2]}

@p{Bir tilki, bir köyün yanındaki derenin kenarında yürüyormuş. Vakit daha
akşam vakti, karanlık çökmek üzereymiş.}

@p{Bu tilkinin karnı çok açıkmış: “Yiyecek bir şey bulabilir miyim acaba?”
deyip, bir tavuk kümesine doğru giderken, bir dere kenarındaki buz üstünde
açılan bir delikten aşağı sarkıtılmış olan bir oltanın sapına gözü takılmış ve
“Dur hele, bakayım” deyip, oltaya doğru yaklaştığında, gerçekten de güneş
batarken sahibinin bırakıp gittiği oltanın suya sarkıtıldığı deliğin üstü henüz
donmamışmış. Tilki ön ayakları ve ağzı ile çekiştirip, oltayı sudan
çıkardığında, ne görsün, olta balık doluymuş. Tilki oltayı orasından burasından
çekiştirerek, balıkları almış ve yakındaki bir ot yığını üzerine yatıp, tadını
çıkara çıkara balık eti yemeye koyulmuş. O civarda gezen aç bir kurt da:
“Yiyecek bir şey denk gelmez mi?” diye etrafı koklayarak,
yürüyormuş. Birdenbire kurdun hassas burnuna balık kokusu gelmiş. Kurt; balık
kokusunu takip edip gelirken, tilkinin bir ot yığını üstünde balık yediğini
görmüş. Kurt, sakin ve dostane bir şekilde:}

@p{— Nasılsın, dostum? demiş.}

@p{Tilki, ağzına doldurduğu balıkları, çiğneyerek:}

@p{— Ç, ç, çok ş, şahane. B, b, bir de ş, şimdi daha da iyi! diye cevap
vermiş.}

@p{— Çok tatlı bir şey yiyiyorsun herhalde, kokusu bütün dünyayı sarmış, demiş
boz kurt ve ağzında dilini emip, toplanan salyaları yutup, yalanmış.}

@p{— Ben mi? Ben balık belişi@numbered-note{Beliş: Hamurun içine et, patates,
pilav vb. konularak pişirelen yemek.} yiyiyorum, demiş Tilki.}

@p{— Mal, insanların eline geçer, nereden, nasıl buldun ki sen onu? demiş
kurt.}

@p{Kurdun gözleri kötü kötü parlamaya başlamış ve balık yiyen tilkiyi,
bütünce yutası gelmiş.}

@p{— Alabilir de, bulabilir de ağabeyin. Al hele sen de tadına bak, demiş tilki
ve kurdu oyalamak, onu kandırıp, oltaya düşürmek için, kurda biraz balık kemiği
fırlatmış. Aç kurt, kemikleri çiğnemeden bütünüyle yutmuş, az daha dilini de
yutacakmış.}

@p{Bundan sonra kurt:}

@p{— Bana da öğretsene, ben de gidip, alayım şunu, diye yalvarmaya başlamış.}

@p{— İyi dinle, öğreteyim, demiş tilki. Az ötede bir delik var. O delik daha
donmamış olacak. Eğer biraz donmuşsa, ayakların ile buzu kır ve kuyruğunu
dibine kadar suya batırıp otur ve hiç kımıldama. Kımıldarsan balıkları
korkutursun, tuzağa düşmezler, demiş. Böyle oturursan, kuyruğunun her tüyü bir
balık yakalayıp, çıkar, demiş.}

@p{Kurt, tilkinin öğrettiği gibi, deliğin yanına varmış ve su üstünde ince olan
buzu ön ayakları ile kırıp, kuyruğunu dibine kadar suya batırmış ve hiç
kımıldamadan oturmuş. Biraz oturduktan sonra, denemek için kuyruğunu biraz
çekince kuyruğunun ağrımaya başladığını sezip: “Oy, iş kötü, balıklar korkup
kaçmasalar iyi olurdu”, deyip, yine rahatça oturmaya başlamış. Uzun bir süre
oturduktan sonra kuyruğunu sudan çıkarmak isteyince, yerinden hiç hareket
ettirememiş. “Vay, çok mu oturdum acaba, balıklar tahminimden de çok mu takıldı
yoksa?” deyip, çekiştirerek kurtulmaya çalıştığında, işin ne olduğunu anlayıp,
kurdun aklı gitmiş. Kuyruğu tamamen buz tutmuş.}

@p{Tan yeri ağarınca, kadınlar deliğe su almak için gelmeye başlamışlar. Su
boyuna indiklerinde bakmışlar ki, delikte bir kurt oturuyor; hemen erkeklere
haber vermişler. Birkaç erkek gelip, kimisi su terazisi, kimisi de sopa ile
kurdu dövmeye başlamışlar. O sırada balıkçı da gelmiş ve elindeki buz kırmaya
yarayan alet ile kurdun alnına bir kaç kez vurunca, kurt cansız bir şekilde
yere yığılmış. Kurdun derisini yüzüp, pazarda satmışlar.}

@p{Tilki, kurdun başına gelenleri, ot yığınının tepesinden gözetleyip, bu
olanlara gülüyormuş.}
