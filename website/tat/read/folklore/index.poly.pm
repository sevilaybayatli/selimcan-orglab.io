#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "Татар фольклоры")
@(define-meta author           "")

@h2{Әкиятләр}

@ul{

@li{@a[#:href "/tat/read/folklore/fox-and-wolf.html"]{Төлке белән бүре | Tilki
ile kurt}}

@li{@a[#:href "/tat/read/folklore/sick-carrying-healthy.html"]{Сауны хаста
күтәрер | Sağlıklıyı hasta taşır}}

@li{@a[#:href "/tat/read/folklore/bear-and-fox.html"]{Аю белән төлке | Ayı ile
tilki}}

@li{@a[#:href "/tat/read/folklore/bear-wolf-fox.html"]{Аю, төлке, бүре | Ayı,
kurt ve tilki}}

@li{@a[#:href "/tat/read/folklore/sly-fox.html"]{Наян төлке | Kurnaz tilki}}

@li{@a[#:href "/tat/read/folklore/lion-fox-wolf.html"]{Арслан, бүре, төлке |
Aslan, tilki ve kurt}}

}
