#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "Tatarca okumayı öğreniyoruz")
@(define-meta author           "")

@script[#:src "../../js/main.js"]

@(define AUDIOS "https://audio.selimcan.org/tat/")

@(define (transcribe t) (string-append "[" t "]"))

@(define (pronounce word) (string-append "«" word "»"))

@(define SOUNDS '("a" "ä" "ya" "yä" "b" "c" "ç" "d" "e" "yı" "ye" "f" "g" "ğ"
  "h" "i" "ı" "j" "k" "l" "m" "n" "ñ" "o" "ö" "p" "q" "r" "s" "ş" "t" "u" "ü"
  "yu" "yü" "v" "w" "x" "y" "z"))

@(for/splice ([s SOUNDS]) @audio[#:id s #:controls "" #:style "display: none"]{
  @source[#:src (string-append AUDIOS s ".wav") #:type
  "audio/ogg"] Tarayıcınız ses öğesini anlamıyor.}  )

@p{Kiril alfabesine dayanan Tatar alfabesi şu şekildedir:}

@ul[#:class "alphabet"]{

 @li{А а}
 @li{Ә ә}
 @li{Б б}
 @li{В в}
 @li{Г г}
 @li{Д д}
 @li{Е е}
 @li{Ё ё}
 @li{Ж ж}
 @li{Җ җ}
 @li{З з}
 @li{И и}
 @li{Й й}
 @li{К к}
 @li{Л л}
 @li{М м}
 @li{Н н}
 @li{Ң ң}
 @li{О о}
 @li{Ө ө}
 @li{П п}
 @li{Р р}
 @li{С с}
 @li{Т т}
 @li{У у}
 @li{Ү ү}
 @li{Ф ф}
 @li{Х х}
 @li{Һ һ}
 @li{Ц ц}
 @li{Ч ч}
 @li{Ш ш}
 @li{Щ щ}
 @li{Ъ ъ}
 @li{Ы ы}
 @li{Ь ь}
 @li{Э э}
 @li{Ю ю}
 @li{Я я}

}

@p{Tatarca okumayı öğrenmek isteyen bir çocuk için bu alfabenin iki zorluğu
vardır:}

@ol{

@li{Tatar dilinde harflerin bir kısmı birkaç farklı sesi ifade eder, yani "bir
harf - bir ses" ilkesi gözetilmez.

@numbered-note{Örneğin, е harfi üç sesi temsil edebilir — @pronounce{тел}
sözünde @transcribe{e}, @pronounce{ел} sözünde @transcribe{yı},
@pronounce{егет} sözünde @transcribe{ye} (transkripsyonları 2012 Latin
alfabesini kullanarak verdik).}

;}

@li{Harflerin bazıları Tatarcada bir sesi, Rusçada başka bir sesi temsil ediyor
ve birçok Tatar çocuğu aynı anda Rusça okumayı öğrendiği için, bu da kafa
karışıklığına neden oluyor.

@numbered-note{Örneğin, ы: @pronounce{ылыс} vs @pronounce{рывок}, г:
@pronounce{гариза} vs @pronounce{гараж} vb.}

.}

}

@p{Bu faktörlerin her ikisi de Tatarca okumayı öğrenmeyi zorlaştırıyor. Bunun
olmasını önlemek için okumayı öğrenenler için özel, basitleştirilmiş bir alfabe
hazırladık. Bu alfabede Tatar dilinde bir harf dışında tüm harfler yalnızca bir
sesi temsil edebilir. Ayrıca Tatarca'da bir, Rusça'da başka bir sesi olan
harfler için özel semboller ekledik.}

@p{Bu anlamda basitleştirilmiş alfabe aşağıdaki gibidir (dinlemek için harfe
tıklayın):

@margin-note{@hyperlink["https://audio.selimcan.org/tat/tatar-sounds.apkg"]{Albabeyi
Anki paketi olarak indirmek}}

}

@ul[#:class "alphabet"]{

 @li[#:onclick "hello_world.core.playSound('a')"]{\(\breve{а}\)}
 @li[#:onclick "hello_world.core.playSound('ä')"]{\(\ddot{а}\)}
 @li[#:onclick "hello_world.core.playSound('ä')"]{ә}
 @li[#:onclick "hello_world.core.playSound('b')"]{б}
 @li[#:onclick "hello_world.core.playSound('w'); hello_world.core.playSound('v')"]{в}
 @li[#:onclick "hello_world.core.playSound('g')"]{г}
 @li[#:onclick "hello_world.core.playSound('ğ')"]{\(\breve{г}\)} 
 @li[#:onclick "hello_world.core.playSound('d')"]{д}
 @li[#:onclick "hello_world.core.playSound('e')"]{\(\breve{е}\)}
 @li[#:onclick "hello_world.core.playSound('yı')"]{\(\bar{е}\)}
 @li[#:onclick "hello_world.core.playSound('ye')"]{\(\ddot{е}\)}
 @li[#:onclick "hello_world.core.playSound('j')"]{ж}
 @li[#:onclick "hello_world.core.playSound('c')"]{җ}
 @li[#:onclick "hello_world.core.playSound('z')"]{з}
 @li[#:onclick "hello_world.core.playSound('i')"]{и}
 @li[#:onclick "hello_world.core.playSound('y')"]{й}
 @li[#:onclick "hello_world.core.playSound('k')"]{к}
 @li[#:onclick "hello_world.core.playSound('q')"]{\(\breve{к}\)} 
 @li[#:onclick "hello_world.core.playSound('l')"]{л}
 @li[#:onclick "hello_world.core.playSound('m')"]{м}
 @li[#:onclick "hello_world.core.playSound('n')"]{н}
 @li[#:onclick "hello_world.core.playSound('ñ')"]{ң}
 @li[#:onclick "hello_world.core.playSound('o')"]{\(\breve{о}\)} 
 @li[#:onclick "hello_world.core.playSound('ö')"]{ө}
 @li[#:onclick "hello_world.core.playSound('p')"]{п}
 @li[#:onclick "hello_world.core.playSound('r')"]{р}
 @li[#:onclick "hello_world.core.playSound('s')"]{с}
 @li[#:onclick "hello_world.core.playSound('t')"]{т}
 @li[#:onclick "hello_world.core.playSound('u')"]{у}
 @li[#:onclick "hello_world.core.playSound('ü')"]{ү}
 @li[#:onclick "hello_world.core.playSound('f')"]{ф}
 @li[#:onclick "hello_world.core.playSound('x')"]{х}
 @li[#:onclick "hello_world.core.playSound('h')"]{һ}
 @li[#:onclick "hello_world.core.playSound('ts')"]{ц}
 @li[#:onclick "hello_world.core.playSound('ç')"]{\(\breve{ч}\)} 
 @li[#:onclick "hello_world.core.playSound('ş')"]{ш}
 @li[#:onclick "hello_world.core.playSound('şç')"]{щ}
 @li[]{ъ}
 @li[#:onclick "hello_world.core.playSound('ı')"]{\(\breve{ы}\)} 
 @li{ь}
 @li[#:onclick "hello_world.core.playSound('e')"]{э} 
 @li[#:onclick "hello_world.core.playSound('yu')"]{\(\bar{ю}\)}
 @li[#:onclick "hello_world.core.playSound('yü')"]{\(\ddot{ю}\)}
 @li[#:onclick "hello_world.core.playSound('ya')"]{\(\bar{я}\)}  
 @li[#:onclick "hello_world.core.playSound('yä')"]{\(\ddot{я}\)}  

}

@p{Bu alfabenin her harfinin sesini 2012 Latin alfabesini kullanarak
gösterirsek, transkripsiyonlar şöyle görünecektir:}

@table[#:class "alphabet-table"
 @tr[@th{Bizim alfabemiz} @th{2012 Latin alfabesi}]
 @tr[@td{\(\breve{а}\)} @td{a}]
 @tr[ @td{\(\ddot{а}\)} @td{ä}]
 @tr[@td{ә}             @td{ä}]
 @tr[@td{б} @td{b}]
 @tr[@td{в} @td{w яки v}]
 @tr[@td{г} @td{g}]
 @tr[@td{\(\breve{г}\)} @td{ğ}]
 @tr[@td{д} @td{d}]
 @tr[@td{\(\breve{е}\)} @td{e}]
 @tr[@td{\(\bar{е}\)} @td{yı}]
 @tr[@td{\(\ddot{е}\)} @td{ye}]
 @tr[@td{ж} @td{j}]
 @tr[@td{җ} @td{c}]
 @tr[@td{з} @td{z}]
 @tr[@td{и} @td{i}]
 @tr[@td{й} @td{y}]
 @tr[@td{к} @td{k}]
 @tr[@td{\(\breve{к}\)} @td{q}]
 @tr[@td{л} @td{l}]
 @tr[@td{м} @td{m}]
 @tr[@td{н} @td{n}]
 @tr[@td{ң} @td{ñ}]
 @tr[@td{\(\breve{о}\)} @td{o}]
 @tr[@td{ө} @td{ö}]
 @tr[@td{п} @td{p}]
 @tr[@td{р} @td{r}]
 @tr[@td{с} @td{s}]
 @tr[@td{т} @td{t}]
 @tr[@td{у} @td{u}]
 @tr[@td{ү} @td{ü}]
 @tr[@td{ф} @td{f}]
 @tr[@td{х} @td{x}]
 @tr[@td{һ} @td{h}]
 @tr[@td{ц} @td{ts}]
 @tr[@td{\(\breve{ч}\)} @td{ç}]
 @tr[@td{ш} @td{ş}]
 @tr[@td{щ} @td{şç}]
 @tr[@td{ъ} @td{_}]
 @tr[@td{\(\breve{ы}\)} @td{ı}]
 @tr[@td{ь} @td{_}]
 @tr[@td{э} @td{e}]
 @tr[@td{\(\bar{ю}\)} @td{yu}]
 @tr[@td{\(\ddot{ю}\)} @td{yü}]
 @tr[@td{\(\bar{я}\)} @td{ya}]
 @tr[@td{\(\ddot{я}\)} @td{yä}]

]

@; TODO x sounds in total. y only in Russian loan words.

@p{Okumayı öğrenen bir öğrenci, bu göreve özel alfabenin harflerini ve
çıkardıkları sesleri tanıyarak başlar. Ders sürecinde harflere eklenen yardımcı
işaretler giderek azalır ve öğrenci yavaş yavaş gerçek Tatar alfabesiyle
yazılmış metinleri okumaya geçer.}

@p{Tüm harflerin birsesli olması mümküm olurdu, ancak deneyimler bunun
yapılacak doğru şey olmadığını gösteriyor -- alfabeyi tamamen düzenli hale
getirirseniz, düzensiz, "gerçek" bir alfabeye geçmekte sorun
yaşayabilirsiniz. Bu yüzden iki sesi temsil etmesi için "v" harfini
bıraktık. 'Вагон' veya 'ваза' gibi türememiş kelimelerde, bu harf [v] sesini
(sesli dudak-diş yaklaşımı), diğer kelimelerde [w] sesini (sesli labiovelar
yaklaşım) temsil eder.}

@;TODO a note on ё , saying that it was deliberate left ambiguous between Tatar
@;and Russian to introduce the kid to the idea that different letters can mean
@;different souns in different languages.

@p{Кечерәк шрифт белән язылган хәрефләр укылмыйлар, мисалы «ь» һәм «ъ».}

@h2{Беренче дәрес}

@h3{Беренче бирем. Яңа аваз белән танышу}

@ol{

@li{@ins{@sound{а}'га төртеп күрсәтегез.} @say{Мин бармагымны бу аваз астына
куеп, аны әйтеп күрсәтәчәкмен.} @ins{Җәянең беренче түгәрәгенә төртеп
күрсәтегез. Тиз генә икенче түгәрәккә күчегез. Ике секунд тотып торыгыз.}
@say{ааа.} @ins{Бармагыгызны җәя буйлап йөртеп бетерегез.}}

@li{@say{Хәзер синең чират. Мин аңа төртеп күрсәткәч, авазны әйт.}
(Бармагыгызны беренче түгәрәккә куегыз.) @say{Әзерлән.} (Икенче түгәрәккә
күчегез. Тотып торыгыз.) @say{«\(\breve{а}\)\(\breve{а}\)\(\breve{а}\)»}}

@li{(Беренче түгәрәкне төртеп күрсәтегез.) @say{Тагын бер кат. Әзерлән.} (Тиз
генә икенче түгәрәккә күчегез. Туктап торыгыз.)
@say{«\(\breve{а}\)\(\breve{а}\)\(\breve{а}\)»}}

}

@img[#:src "../../img/a-back.png" #:alt "back vowel a"]
