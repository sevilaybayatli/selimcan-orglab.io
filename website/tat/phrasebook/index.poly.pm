#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "Татарча сөйләүлек | Tatarca konuşma kılavuzu")
@(define-meta author           "")

@h2{Күрешү, саубуллашу | Selamlar, vedalar}

@div[#:class "page"]{
 @div[#:class "lefthalf"]{@p{Исәнме!@margin-note{"Син" дип эндәшкән кешегә әйтелә.}}}
 @div[#:class "righthalf"]{@p{Merhaba!@margin-note{"Sen" diye hitap ettiğiniz
 kişiye.}}}
 
 @div[#:class "lefthalf"]{@p{Исәнмесез!@margin-note{"Сез" дип эндәшкән кешегә әйтелә.}}}
 @div[#:class "righthalf"]{@p{Merhaba!@margin-note{"Siz" diye hitap ettiğiniz kişiye.}}}

 @div[#:class "lefthalf"]{@p{Сәлам!}}
 @div[#:class "righthalf"]{@p{Selam!}}

 @div[#:class "lefthalf"]{@p{Хәерле иртә!}}
 @div[#:class "righthalf"]{@p{Günaydın!}}

 @div[#:class "lefthalf"]{@p{Хәерле көн!}}
 @div[#:class "righthalf"]{@p{İyi günler!}}

 @div[#:class "lefthalf"]{@p{Хәерле кич!}}
 @div[#:class "righthalf"]{@p{İyi akşamlar!}}

 @div[#:class "lefthalf"]{@p{Тыныч йокы!}}
 @div[#:class "righthalf"]{@p{İyi uykylar!}}

 @div[#:class "lefthalf"]{@p{Сау бул!}}
 @div[#:class "righthalf"]{@p{Hoşça kal!}}

 @div[#:class "lefthalf"]{@p{Сау булыгыз!}}
 @div[#:class "righthalf"]{@p{Hoşça kalın!}}

 @div[#:class "lefthalf"]{@p{Рәхмәт!}}
 @div[#:class "righthalf"]{@p{Teşekkürler!}}

}



