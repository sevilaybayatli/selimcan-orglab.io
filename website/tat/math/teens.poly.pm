#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Teens} @tat{Уннан алып унтугызга кадәрге саннар}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/JmGaRcRtOuU"
         #:english "https://www.youtube-nocookie.com/embed/ourH3ueWNmA"
         #:tatar "https://www.youtube-nocookie.com/embed/ourH3ueWNmA"]{@video-title{@u{@eng{Teens as sums with 10} @tat{Уннан алып унтугызга кадәрге саннарны 10 [һәм тагын башка бер сан] суммасы итеп күрсәтү}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/NvJ9kcXHoYM"
         #:english "https://www.youtube-nocookie.com/embed/zqwVKhQV_2w"
         #:tatar "https://www.youtube-nocookie.com/embed/zqwVKhQV_2w"]{@video-title{@u{@eng{Monkeys for a party} @tat{Маймыллар кичәгә}}}}
         
@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-place-value-topic/cc-early-math-teens/e/teen-numbers-1"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Teen numbers} @tat{Күнегү: уннан алып унтугызга кадәрге саннар}}}}}
                                                                       
