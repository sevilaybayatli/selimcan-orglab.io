#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Tens} @tat{Дистәләр}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/V9lxh96BaZ0" 
         #:english "https://www.youtube-nocookie.com/embed/wx2gI8iwMCA"
         #:tatar "https://www.youtube-nocookie.com/embed/wx2gI8iwMCA"]{@video-title{@u{@eng{Intro to place value} @tat{Урын кыймәтенә кереш}}}}
    
@kavideo[#:frank "https://www.youtube-nocookie.com/embed/ziU_n6wFOR4"
         #:english "https://www.youtube-nocookie.com/embed/X_PnRFAKbkg"
         #:tatar "https://www.youtube-nocookie.com/embed/X_PnRFAKbkg"]{@video-title{@u{@eng{Place value example: 25} @tat{Урын кыйммәте[нә] мисал: 25}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/HvJy5R-Y1OQ"
         #:english "https://www.youtube-nocookie.com/embed/-Zlq5tNl94M"
         #:tatar "https://www.youtube-nocookie.com/embed/-Zlq5tNl94M"]{@video-title{@u{@eng{Place value example: 42} @tat{Урын кыйммәте[нә] мисал: 42}}}}
         
@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-place-value-topic/cc-early-math-tens/e/groups-of-tens"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Groups of ten objects} @tat{Күнегү: ун әйбер төркемнәре}}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-place-value-topic/cc-early-math-tens/e/tens-and-ones"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Tens and ones} @tat{Күнегү: дистәләр һәм берәмлекләр}}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-place-value-topic/cc-early-math-tens/e/understanding-2-digit-numbers"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: 2-digit place value challenge} @tat{Күнегү: 2 цифралы [саннарда] урын кыйммәте мәсьәләсе}}}}}
