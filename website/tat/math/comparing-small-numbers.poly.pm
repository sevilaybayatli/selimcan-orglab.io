#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Comparing small numbers} @tat{Кечкенә саннарны чагыштыру}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/aTyCJW1rmpQ"
         #:english "https://www.youtube-nocookie.com/embed/__nkbr6DeTg"
         #:tatar "https://www.youtube-nocookie.com/embed/__nkbr6DeTg"]{@video-title{@u{@eng{Comparing numbers of objects} @tat{Әйберләрнең санын чагыштыру}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-comparing-numbers/e/compare-groups-through-10" #:target "_blank"]{@practice-title{@u{@eng{Practice: Compare numbers of objects 1} @tat{Күнегү: әйберләрнең санын чагыштыр 1}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/bNjPC2rqMY4"
         #:english "https://www.youtube-nocookie.com/embed/tJrSILRXOUc"
         #:tatar "https://www.youtube-nocookie.com/embed/tJrSILRXOUc"]{@video-title{@u{@eng{Comparing numbers on the number line} @tat{Саннар турысындагы саннарны чагыштыру}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-comparing-numbers/e/comparing-numbers-through-10" #:target "_blank"]{@practice-title{@u{@eng{Practice: Compare numbers to 10} @tat{Күнегү: 10-га кадәрге саннарны чагыштыр}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/aJXOzcJTqhg"
         #:english "https://www.youtube-nocookie.com/embed/UA975j_qsTQ"
         #:tatar "https://www.youtube-nocookie.com/embed/UA975j_qsTQ"]{@video-title{@u{@eng{Counting by category} @tat{Төркемләп санау}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-comparing-numbers/e/sort-groups-by-count" #:target "_blank"]{@practice-title{@u{@eng{Practice: Compare numbers of objects 2} @tat{Күнегү: әйберләрнең санын чагыштыр 2}}}}}
