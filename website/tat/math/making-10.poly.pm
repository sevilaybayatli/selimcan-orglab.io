#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Making 10} @tat{10-ны хасил итү}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/CP2wqyukJbk"
         #:english "https://www.youtube-nocookie.com/embed/An46SYAxhtc"
         #:tatar "https://www.youtube-nocookie.com/embed/An46SYAxhtc"]{@video-title{@u{@eng{Getting to 10 by filling boxes} @tat{Тартмаларны тутырып, 10-га җитү}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-make-10/e/making-ten"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Make 10 (grids and number bonds)} @tat{Күнегү: 10-ны хасил ит (җәдвәлләр һәм сан парлары)}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/Nx7TU8RzkL8"
         #:english "https://www.youtube-nocookie.com/embed/9FC0WT186aY"
         #:tatar "https://www.youtube-nocookie.com/embed/9FC0WT186aY"]{@video-title{@u{@eng{Adding to 10} @tat{Ун булырлык итеп кушу: "унга кушу"}}}}
             
@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-make-10/e/making-ten-2"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Make 10} @tat{Күнегү: 10-ны хасил ит}}}}}
             
