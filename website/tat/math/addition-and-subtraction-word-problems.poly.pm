#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Addition and subtraction word problems} @tat{Кушу һәм алу[га] сүз [белән бирелгән] мәсьәләләр}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/vNP7hNURNHE"
         #:english "https://www.youtube-nocookie.com/embed/-3DFzxbP9Fk"
         #:tatar "https://www.youtube-nocookie.com/embed/-3DFzxbP9Fk"]{@video-title{@u{@eng{Addition word problems within 10} @tat{10-га кадәр[ге саннарны] кушу[га] сүз [белән бирелгән] мәсьәләләр}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-add-sub-word-problem-within-10/e/addition-word-problems-within-10"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Addition word problems within 10} @tat{Күнегү: 10-га кадәр[ге саннарны] кушу[га] сүз [белән бирелгән] мәсьәләләр}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/cSLX2yEfW4Q"
         #:english "https://www.youtube-nocookie.com/embed/qSkpZswoZTc"
         #:tatar "https://www.youtube-nocookie.com/embed/qSkpZswoZTc"]{@video-title{@u{@eng{Subtraction word problems within 10} @tat{10-га кадәр[ге саннарны] алу[га] сүз [белән бирелгән] мәсьәләләр}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-add-sub-word-problem-within-10/e/subtraction-word-problems-within-10"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Subtraction word problems within 10} @tat{Күнегү: 10-га кадәр[ге саннарны] алу[га] сүз [белән бирелгән] мәсьәләләр}}}}}
