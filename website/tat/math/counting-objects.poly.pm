#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Counting objects} @tat{Затларны санау}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/--ZIp7bzQVo"
         #:english "https://www.youtube-nocookie.com/embed/leDYnoNSvD4"
         #:tatar "https://www.youtube-nocookie.com/embed/leDYnoNSvD4"]{@video-title{@u{@eng{Counting in pictures} @tat{Рәсемнәрне санау}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-count-object-topic/e/counting-in-scenes" #:target "_blank"]{@practice-title{@u{@eng{Practice: Count in pictures} @tat{Күнегү: рәсемнәрне сана}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/KdF8zKJQVeA"
         #:english "https://www.youtube-nocookie.com/embed/I9S5CvSqb5A"
         #:tatar "https://www.youtube-nocookie.com/embed/I9S5CvSqb5A"]{@video-title{@u{@eng{Counting objects 1} @tat{Затларны [әйбер я җан ияләрен] санау 1}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-count-object-topic/e/how-many-objects-1" #:target "_blank"]{@practice-title{@u{@eng{Practice: Count objects 1} @tat{Күнегү: затларны [әйбер я җан ияләрен] сана 1}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/uO_Ps8LxX30"
         #:english "https://www.youtube-nocookie.com/embed/EUqhLxFccbM"
         #:tatar "https://www.youtube-nocookie.com/embed/EUqhLxFccbM"]{@video-title{@u{@eng{Counting objects 2} @tat{Затларны [әйбер я җан ияләрен] санау 2}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-count-object-topic/e/how-many-objects-2" #:target "_blank"]{@practice-title{@u{@eng{Practice: Count objects 2} @tat{Күнегү: затларны [әйбер я җан ияләрен] сана 2}}}}}
