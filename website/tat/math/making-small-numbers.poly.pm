#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Making small numbers} @tat{Кечкенә саннар хасил итү}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/PsD8sEgrtNg"
         #:english "https://www.youtube-nocookie.com/embed/A-ykhY_IoaU"
         #:tatar "https://www.youtube-nocookie.com/embed/A-ykhY_IoaU"]{@video-title{@u{@eng{Making 5} @tat{5-не хасил итү}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-making-5-9/e/making-five"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Make 5} @tat{Күнегү: 5-не хасил ит}}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-making-5-9/e/making-totals-in-different-ways-within-10"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Make small numbers in different ways} @tat{Күнегү: төрле ысуллар белән кечкенә саннар булдыр}}}}}
             
