.PHONY : publish

js: website/hello-world/src/hello_world/core.cljs
	cd website/hello-world/ && clj -M -m cljs.main --optimizations advanced -c hello-world.core
publish: js
	rm -rf publish; cd website; raco pollen render; raco pollen publish; cd ..; mv ~/publish .; cd publish; ln -sf tat/index.html index.html
	find publish/ -name "*.html" -type f -exec echo {} \; -exec tidy -config tidy-config.txt -i -w 79 -ashtml -m {} \;
	mkdir -p publish/js
	cp website/hello-world/out/main.js publish/js/main.js
serve: publish
	cd publish; python3 -m http.server 1234

clean:
	rm -rf publish/
	find website/ -name "compiled" -type d -exec rm -rf {} \;
	find . -name "*~" -type f -exec rm -rf {} \;
