#!/usr/bin/env bash

## A script to set up ka-lite on a new Ubuntu server.
## Tested on Ubuntu 17.10.

adduser selimcan
export EDITOR="nano"
printf "Add the following line into the file which will open next:\n"
printf "selimcan\tALL=(ALL:ALL) ALL\n"
sleep 10
visudo

sudo -u selimcan bash << HARGLE
mkdir .ssh
printf "Copy-paste your public ssh key into the file which will open next.\n"
sleep 10
nano .ssh/authorized_keys
HARGLE

sudo apt update && sudo apt upgrade
sudo apt install nodejs npm python-pip gettext
sudo apt autoremove && sudo apt autoclean && sudo apt clean

git clone https://gitlab.com/selimcan-org/ka-lite.git
sudo pip install virtualenvwrapper
source `which virtualenvwrapper.sh`
mkvirtualenv kalite
workon kalite
cd ka-lite
pip install -e .
pip install -r requirements_dev.txt
kalite manage setup
wget http://pantry.learningequality.org/downloads/ka-lite/0.17/content/contentpacks/de.zip
kalite manage retrievecontentpack local de de.zip
kalite restart

# sudo apt-get install software-properties-common
# sudo su -c 'echo "deb http://ppa.launchpad.net/learningequality/ka-lite/ubuntu artful main" > /etc/apt/sources.list.d/ka-lite.list'
# sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 74F88ADB3194DD81
# sudo apt-get update && sudo apt-get upgrade && sudo apt-get autoremove && sudo apt-get clean
# sudo apt-get install ka-lite

printf "KA Lite should be running now on localhost:8008\n"

